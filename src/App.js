import React from 'react';
import { SafeAreaView,View,Text,Image,TextInput,StyleSheet, Dimensions, ScrollView, StatusBar, ActivityIndicator, Button, Alert, ImageBackground, TouchableOpacity} from 'react-native'

import ComponentSejajar from './components/ComponentSejajar';

const App = () => (
    <SafeAreaView style={{ backgroundColor: 'white',flex:1 ,flexDirection: 'row',
    justifyContent:'space-evenly',
    alignItems:'center'}}>
    <ComponentSejajar/>
    </SafeAreaView>
);

export default App;
